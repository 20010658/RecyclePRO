package com.example.marco.recyclepro;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.widget.ProgressBar;




import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;


import static com.example.marco.recyclepro.Excetera.*;


public class LoadingScreen extends Activity {
    private ProgressBar loading;
    private String jsonGenerated;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);

        loading = (ProgressBar) findViewById(R.id.pbar);
        loading.animate();
        LoadingTask task = new LoadingTask();
        task.execute();
    }

    //legge l'excel per trasformare le informazioni di garbage in Json
    public String fetchInfoGarbage() throws IOException, JSONException {

        InputStream stream = getResources().openRawResource(R.raw.elenco);
        JSONObject json = new JSONObject();
        XSSFWorkbook workbook = new XSSFWorkbook(stream);


        Sheet sheet = workbook.getSheetAt(0);

        JSONArray rows = new JSONArray();


        boolean first=true;


        for (Iterator<Row> rowsIterator = sheet.rowIterator(); rowsIterator.hasNext();)
        {

            if (first) {
                rowsIterator.next();
                first = false;
            }

            Row row = rowsIterator.next();


            JSONObject cells = new JSONObject();

            int i=1;

            //itera sulle celle
            for (Iterator<Cell> cellsIterator = row.cellIterator(); cellsIterator.hasNext();)
            {
                Cell cell = cellsIterator.next();
                switch(i){
                    case 1:
                        cells.put(W_TYPE, cell.getStringCellValue());
                        break;
                    case 2:
                        if (row.getPhysicalNumberOfCells()==4){
                            cells.put(W_INFO, cell.getStringCellValue());
                        } else {//salta la lettura e resetta il contatore nel caso in cui la riga è da tre elementi pieni(manca la descrizione)
                            cells.put(W_INFO, "");
                            cellsIterator=row.cellIterator();
                            cell = cellsIterator.next();
                        }
                        break;
                    case 3:
                        cells.put(W_WHERE, cell.getStringCellValue());
                        break;
                    case 4:
                        String[] keys = cell.getStringCellValue().split(", ");
                        JSONArray keyslist = new JSONArray();
                        for (int x = 0; x<keys.length; x++)
                            keyslist.put(keys[x]);
                        cells.put(W_KEYS, keyslist);
                        break;
                }
                i++;
            }
            rows.put(cells);
        }

        // Crea JSON.
        json.put(W_TITLE, rows);

        // restituisce il testo
        return json.toString();
    }
    private String fetchInfoCities() throws IOException, JSONException {

        InputStream stream = getResources().openRawResource(R.raw.elenco);
        JSONObject json = new JSONObject();
        XSSFWorkbook workbook = new XSSFWorkbook(stream);

        Sheet sheet = workbook.getSheetAt(1);

        JSONArray rows = new JSONArray();

        boolean first=true;

        for (Iterator<Row> rowsIterator = sheet.rowIterator(); rowsIterator.hasNext();)
        {
            if (first) {
                rowsIterator.next();
                first = false;
            }
            Row row = rowsIterator.next();

            JSONObject cells = new JSONObject();
            int i=1;
            for (Iterator<Cell> cellsIterator = row.cellIterator(); cellsIterator.hasNext();)
            {
                Cell cell = cellsIterator.next();
                switch(i){
                    case 1:
                        cells.put(citta, cell.getStringCellValue());
                        break;
                    case 2:
                        cells.put(umido, cell.getStringCellValue());
                        break;
                    case 3:
                        cells.put(indif, cell.getStringCellValue());
                        break;
                    case 4:
                        cells.put(ingom, cell.getStringCellValue());
                        break;
                }
                i++;
            }
            rows.put(cells);
        }

        json.put(titoloCITT, rows);

        return json.toString();
    }


    //asynctask
    private class LoadingTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                jsonGenerated=fetchInfoGarbage();
                rWaste= new JSONObject(jsonGenerated);
                jsonGenerated=fetchInfoCities();
                rCities= new JSONObject(jsonGenerated);
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Intent in = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(in);
            LoadingScreen.this.finish();
        }
    }
}
