package com.example.marco.recyclepro;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.marco.recyclepro.Excetera.*;


public class CityActivity extends Activity implements TextWatcher, View.OnClickListener {
    private JSONObject reader;
    private static String[] items;
    private LinearLayout results;
    private ImageButton search, delete;
    private AutoCompleteTextView input;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        //i dati ci sono già, sono in Excetera
        reader= rCities;

        //riempie il vettore con le parole da ricercare in input
        prepareItems();
        input = (AutoCompleteTextView)findViewById(R.id.cityInput);
        input.addTextChangedListener(this);

        //setta input in modo da filtrare gli elementi in items
        input.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, items));

        search=(ImageButton) findViewById(R.id.citySearch);
        search.setOnClickListener(this);
        delete=(ImageButton) findViewById(R.id.deleteField);
        delete.setOnClickListener(this);

        results=(LinearLayout)findViewById(R.id.cityResults);
    }

    //legge i nomi delle città e li inserisce in items
    private void prepareItems() {
        String k= "";
        try {
            JSONArray types = reader.getJSONArray(titoloCITT);
            for (int i=0; i<types.length(); i++){
                k= k + "-" + types.getJSONObject(i).getString(citta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        items=k.split("-");
    }

    //dato il nome di una città, crea una vista con le informazioni necessarie
    private void retreiveInfo() {
        boolean ok = false;
        int i=0;
        results.removeAllViews();
        try {
            JSONArray types = reader.getJSONArray(titoloCITT);
            while (i<types.length() && !ok){
                if (types.getJSONObject(i).getString(citta).equals(input.getText().toString())){
                    View v = getLayoutInflater().inflate(R.layout.table_row_city, null);
                    TextView temp = (TextView) v.findViewById(R.id.humidInfo);
                    temp.setText(types.getJSONObject(i).getString(umido));
                    temp = (TextView) v.findViewById(R.id.genericInfo);
                    temp.setText(types.getJSONObject(i).getString(indif));
                    temp = (TextView) v.findViewById(R.id.bigInfo);
                    temp.setText(types.getJSONObject(i).getString(ingom));
                    temp = (TextView) v.findViewById(R.id.title);
                    temp.setText(types.getJSONObject(i).getString(citta));
                    results.addView(v);
                    ok=true;
                }
                i++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(!ok)
            Toast.makeText(this, R.string.infoNotFound, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.infoFound, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.citySearch:
                retreiveInfo();
                break;
            case R.id.deleteField:
                input.setText("");
                break;
        }

    }
}
