package com.example.marco.recyclepro;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import static com.example.marco.recyclepro.Excetera.*;


public class WasteActivity extends Activity implements TextWatcher, View.OnClickListener {

    // VARIABILI
    private AutoCompleteTextView input;
    private static String[] items;
    private JSONObject reader;
    private LinearLayout results;
    private ImageButton search, delete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waste1);

        //i dati ci sono già, sono in Excetera
        reader = rWaste;

        //riempie il vettore con le parole da ricercare in input
        prepareItems();
        input = (AutoCompleteTextView)findViewById(R.id.wasteInput);
        input.addTextChangedListener(this);

        //setta input in modo da filtrare gli elementi in items
        input.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line,
                items));

        search=(ImageButton) findViewById(R.id.wasteSearch);
        search.setOnClickListener(this);
        delete=(ImageButton) findViewById(R.id.deleteField);
        delete.setOnClickListener(this);

        results=(LinearLayout)findViewById(R.id.wasteResults);
    }

    //le chiavi nei vettori dei tipi di rifiuti e le inserisce in items
    private void prepareItems() {
        String k= "";
        try {
            JSONArray types = reader.getJSONArray(W_TITLE);
            for (int i=0; i<types.length(); i++){
                JSONArray keys = types.getJSONObject(i).getJSONArray(W_KEYS);
                for (int j=0; j<keys.length(); j++){
                    k= k + "-" + keys.getString(j);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        items=k.split("-");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.wasteSearch:
                retreiveInfo();
                break;
            case R.id.deleteField:
                input.setText("");
                break;
        }
    }

    //data una chiave, crea una vista con le informazioni necessarie filtrando nelle chiavi dei rifiuti
    private void retreiveInfo() {
        boolean ok = false;
        int i=0, j=0;
        results.removeAllViews();
        try {
            JSONArray types = reader.getJSONArray(W_TITLE);
            while (i<types.length() && !ok){
                JSONArray keys = types.getJSONObject(i).getJSONArray(W_KEYS);
                while (j<keys.length() && !ok){
                    if (keys.getString(j).equals(input.getText().toString())){
                        View v = getLayoutInflater().inflate(R.layout.table_row, null);
                        TextView temp = (TextView) v.findViewById(R.id.type);
                        temp.setText(types.getJSONObject(i).getString(W_TYPE));
                        temp = (TextView) v.findViewById(R.id.description);
                        if(types.getJSONObject(i).getString(W_INFO).equals(""))
                            temp.setText(R.string.nothing);
                        else
                            temp.setText(types.getJSONObject(i).getString(W_INFO));
                        temp = (TextView) v.findViewById(R.id.where);
                        temp.setText(types.getJSONObject(i).getString(W_WHERE));
                        results.addView(v);
                        ok=true;
                    }
                    j++;
                }
                j=0;
                i++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(!ok)
            Toast.makeText(this, R.string.infoNotFound, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, R.string.infoFound, Toast.LENGTH_SHORT).show();
    }
}
