package com.example.marco.recyclepro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttCity, buttWaste;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttCity = (Button) findViewById(R.id.cityButton);
        buttWaste = (Button)findViewById(R.id.whereButton);

        buttCity.setOnClickListener(this);
        buttWaste.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent in;
        switch (view.getId()){
            case R.id.cityButton:
                in = new Intent(getApplicationContext(), CityActivity.class);
                startActivity(in);
                break;
            case R.id.whereButton:
                in = new Intent(getApplicationContext(), WasteActivity.class);
                startActivity(in);
                break;
        }


    }
}
